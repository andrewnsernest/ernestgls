import os
import distutils.dir_util
import shutil

count = 0
filenames = []

myScriptPath = os.path.dirname(os.path.realpath(__file__))
myDroidtexmfPath = "/storage/emulated/0/Android/data/lah.texportal.donate/files/texmf-var/tex/latex/ernestgls"
myLinuxtexmfPath = "/home/anernest/texmf/tex/latex/ernestgls"
myProjectFile = os.path.join(os.pardir,"ernestgls.tex")

for (dirname, dirs, files) in os.walk(myScriptPath):
   for filename in files:
       if filename == 'gnugget.tex' :
           #print os.path.join(dirname,filename)
           filenames.append(os.path.join(dirname,filename))
           count = count + 1
#print filenames
print 'Files:', count

with open('ernestgls.tex', 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            for line in infile:
                #print line
                outfile.write(line)

if os.path.isdir(myDroidtexmfPath):
    print "Droid:", myDroidtexmfPath
    distutils.dir_util.copy_tree(myScriptPath,myDroidtexmfPath, preserve_mode=0, preserve_times=0 )
if os.path.isdir(myLinuxtexmfPath):
    print "Linux:", myLinuxtexmfPath
    distutils.dir_util.copy_tree(myScriptPath,myLinuxtexmfPath, preserve_mode=0, preserve_times=0)
if os.path.exists(myProjectFile):
    print "Project:", myProjectFile
    shutil.copy("ernestgls.tex",myProjectFile)
